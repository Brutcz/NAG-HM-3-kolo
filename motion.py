import RPi.GPIO as GPIO
import time,sys

sensor = 21

GPIO.setmode(GPIO.BOARD)
GPIO.setup(sensor, GPIO.IN, GPIO.PUD_UP)

previous_state = False
current_state = False
try:
	while True:
		time.sleep(0.1)
		previous_state = current_state
		current_state = GPIO.input(sensor)
		if current_state != previous_state:
        		new_state = "HIGH" if current_state else "LOW"
        		print("GPIO pin %s is %s" % (sensor, new_state))
except:
	pass
finally:
	GPIO.cleanup(sensor)
